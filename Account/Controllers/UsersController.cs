using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Account.Data;
using Account.Models;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Account.Controllers;

[Route("api/Account/[controller]")]
[ApiController]
public class UsersController : Controller
{
    private readonly DataDbContext _context;
    private readonly UserManager<AppUser> _userManager;
    private readonly SignInManager<AppUser> _signManager;

    public UsersController(DataDbContext context,
                        UserManager<AppUser> userManager,
                        SignInManager<AppUser> signManager)
    {
        _context = context;
        _userManager = userManager;
        _signManager = signManager;
    }

    // GET: api/Account/Users
    [HttpGet]
    public async Task<ActionResult<UserInfo>> GetUser()
    {
        var user = await _userManager.GetUserAsync(User);
        if (user == null)
            return Unauthorized();

        UserInfo info = new UserInfo();
        info.FromAuthUser(user);
        return info;
    }

    // POST: api/Account/Users
    [HttpPost]
    public async Task<ActionResult<UserInfo>> PostUser(RegisterModel model)
    {
        AppUser user = new AppUser();
        user.FirstName = model.FirstName;
        user.LastName = model.LastName;
        user.Email = model.Email;
        user.UserName = model.Email;
        user.EmailConfirmed = true;
        var result = await _userManager.CreateAsync(user, model.Password);
        if(!result.Succeeded)
            return BadRequest();
        await _context.SaveChangesAsync();
        return CreatedAtAction("GetUser", new { login = user.Email }, user);
    }

    // DELETE: api/Account/Users
    [HttpDelete]
    public async Task<IActionResult> DeletePost()
    {
        var user = await _userManager.GetUserAsync(User);
        if (user == null)
            return Unauthorized();

        await _signManager.SignOutAsync();
        _context.Users.Remove(user);
        await _context.SaveChangesAsync();
        return NoContent();
    }
}