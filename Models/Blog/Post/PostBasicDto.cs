using System;

namespace Blog.Models;

public class PostBasicDto
{
    public int Id { get; set; }
    public String? Title { get; set; }
    public String? Subtitle { get; set; }
    public String? Slug { get; set; }
}