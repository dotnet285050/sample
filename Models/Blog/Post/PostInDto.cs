namespace Blog.Models;

public class PostInDto
{
    public int Id { get; set; }
    public String? Title { get; set; }
    public String? Subtitle { get; set; }
    public ICollection<PostContentDto> Contents { get; set; } = new List<PostContentDto>();
}