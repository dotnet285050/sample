using Microsoft.EntityFrameworkCore;

namespace Blog.Models;

public class BlogContext : DbContext
    {
    public DbSet<Post> Posts { get; set; } = null!;
    public DbSet<PostContent> PostContents { get; set; } = null!;
    public DbSet<PostComment> PostComments { get; set; } = null!;

    public BlogContext(DbContextOptions<BlogContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Post>()
            .HasMany(e => e.Contents)
            .WithOne(e => e.Post)
            .HasForeignKey(e => e.PostId)
            .HasPrincipalKey(e => e.Id)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);
    }
}
