using System;

namespace Blog.Models;

public class PostOutDto
{
    public int Id { get; set; }
    public String? Title { get; set; }
    public String? Subtitle { get; set; }
    public String? Slug { get; set; }
    public ICollection<PostContentDto> Contents { get; set; } = new List<PostContentDto>();
    public ICollection<PostCommentDto> Comments { get; set; } = new List<PostCommentDto>();
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}