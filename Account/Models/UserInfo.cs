using Account.Data;

namespace Account.Models;

public class UserInfo
{
    public String? FirstName { get; set; }
    public String? LastName { get; set; }
    public String? Email { get; set; }
    public DateTime LastLogin { get; set; }

    public void FromAuthUser(AppUser user)
    {
        FirstName = user.FirstName;
        LastName = user.LastName;
        Email = user.Email;
        LastLogin = user.LastLogin;
    }
}