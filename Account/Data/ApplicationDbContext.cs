﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Account.Data;

public class AppUser : IdentityUser
{
    public String? FirstName { get; set; }
    public String? LastName { get; set; }
    public bool IsActive { get; set; }
    public DateTime LastLogin { get; set; }
}

public class DataDbContext : IdentityDbContext<AppUser>
{
    public DataDbContext(DbContextOptions<DataDbContext> options)
        : base(options)
    {
    }
}