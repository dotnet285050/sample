using System.ComponentModel.DataAnnotations;

namespace Blog.Models;


public class Post
{
    [Key]
    public int Id { get; set; }
    [Required]
    public String? Title { get; set; }
    [Required]
    public String? Subtitle { get; set; }
    public String? Slug { get; set; }
    [Required]
    public ICollection<PostContent> Contents { get; set; } = new List<PostContent>();
    public ICollection<PostComment> Comments { get; set; } = new List<PostComment>();
    [Required]
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}