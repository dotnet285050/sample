using System;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models;

public class PostContent
{
    [Key]
    public int Id { get; set; }
    [Required]
    public String? Title { get; set; }
    [Required]
    public String? Content { get; set; }
    [Required]
    public int PostId { get; set; }
    public Post Post { get; set; } = null!;
}