# SAMPLE

Przykład wykorzystania .NET na linux z wykorzystaniem kontenera Dokcer i Visual Studio Code. [tutorial](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-7.0&tabs=visual-studio-code)

### Tworzenie projektu

Aby utworzyć nowy projekt należy wewnątrz kontenera wydać polecenie:
```
export PROJECT_NAME=NewProject
dotnet new webapi --name ${PROJECT_NAME} --output ${PROJECT_NAME}
```

Następnie należy otworzyć nowo utworzony projekt poprzez VSCode i wcisnąć tak dla komunikatu: "Required assets to build and debug are missing from 'NewProject'. Add them?" Pozwoli to na zbudowanie oraz debugowanie projektu.

### Konfiguracja SSL

W linux nie można dodać wygenerowanego certyfikatu tak jak ma to miejsce w przypadku Windows czy IOs z poziomu dotnet. Dlatego na początek podczas testów dobrze go wyłączyć. W pliku Program.cs należy zakomentować linijkę 'app.UseHttpsRedirection();' oraz w pliku Properties/launchSettings.json edytować obiekt profiles -> applicationUrl i zostawić tylko na adres http, a także zmienić localhost na konkretny adres ip kontenera np: 172.17.0.2:3000. Adres można odczytać z pliku /etc/hosts.

### Test ustawień

Po przejściu przez poprzednie kroki powinna być możliwość uruchomienia i przetestowania projektu w przeglądarce na hoście dockera pod adresem: localhost:3000/swagger/index.html

## Development

### Konfiguracja EntityFramework

EntityFramework jest biblioteką typu ORM (Object-Relational Mapping) słążącą do budowania aplikacji opartych o bazy danych.

```
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
```

Przed możliwością generowania kontrolerów należy utworzyć pliku Context bazy danych przy użyciu EntityFramework oraz skonfigurować plik główny aplikacji Program.cs.

Do testów można dodać bazę danych przechowywaną  w pamięci:
```
dotnet add package Microsoft.EntityFrameworkCore.InMemory
```

lub użyć bazy sqlite:
```
dotnet tool install dotnet-ef --global
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
```

W przypadku użycia bazy sqlite należy wykonywać migrację bazy:
```
dotnet-ef migrations add CreateIdentitySchema --context Context
dotnet-ef database update --context Context
```
Pozwoli to w pierwszym kroku na utworzenie poleceń SQL do wygwnwrowania table, następnie na aktualizację tabeli w bazie danych.
W celu odwrócenia zmian wykonanych przez polecenie update można użyć:
```
dotnet-ef database remove --context Context
```

### Konfiguracja generatora plików

```
dotnet tool install -g dotnet-aspnet-codegenerator
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 6.0
```

Kontener domyślnie nie ma ustawionej ścieżki PATH na narzędzia dotnet. Można to wykonać jednorazowo i powtarzać po każdym restarcie kontenera poleceniem:

```
PATH=$PATH:/root/.dotnet/tools
```

Lub edytować plik .bash_profile:
```
cat << EOF >> /root/.bash_profile
# Add .NET Core SDK tools
export PATH=$PATH:/root/.dotnet/tools 
EOF
```

### Generowanie kontrolerów

Aby wygenrować kontroler z istniejącego modelu oraz contextu należy wykonać polecenie, podmieniając NAME na modelu np POST:
```
export CTX_NAME=Blog
export MODEL_NAME=Post
dotnet aspnet-codegenerator controller -name ${MODEL_NAME}sController -async -api -m ${MODEL_NAME} -dc ${CTX_NAME}Context -outDir Controllers
```


### Podłączenie do bazy danych MySQL

Potrzebna jest wtycka do EntityFramework dla bazy mysql:
```
dotnet add package Pomelo.EntityFrameworkCore.MySql
```

[Instrukcja konfiguracji](https://supunawa.medium.com/mysql-database-with-net-core-6-and-entity-framework-cc901bde9127)

### Uwierzytelnianie użytkowników

Wymagane zależności:
```
dotnet add package Microsoft.AspNetCore.Identity
dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore --version 6.0
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
```

[webapi auth](https://stackoverflow.com/questions/43224177/how-to-add-asp-net-identity-to-asp-net-core-when-webapi-template-is-selected)

[Identity](https://learn.microsoft.com/pl-pl/aspnet/core/security/authentication/scaffold-identity?view=aspnetcore-7.0&tabs=netcore-cli)

[Identity](https://learn.microsoft.com/pl-pl/aspnet/core/security/authentication/identity?view=aspnetcore-7.0&tabs=netcore-cli)
