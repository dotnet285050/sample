using System.ComponentModel.DataAnnotations;

namespace Account.Models;

public class LoginModel
{
    [Required]
    public String Email { get; set; } =null!;
    [Required]
    public String? Password { get; set; }
}