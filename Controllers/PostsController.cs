using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Blog.Models;
using Microsoft.AspNetCore.Authorization;

namespace Sample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly BlogContext _context;

        public PostsController(BlogContext context)
        {
            _context = context;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostBasicDto>>> GetPosts()
        {
            return await _context.Posts
                .Select(p => ToBasicDto(p))
                .ToListAsync();
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PostOutDto>> GetPost(int id)
        {
            var post = await _context.Posts
                .Include(e => e.Contents)
                .Include(e => e.Comments)
                .FirstOrDefaultAsync(e => e.Id == id);
            if (post == null)
                return NotFound();

            PostOutDto dto = ToOutDto(post);
            return dto;
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, PostInDto postDto)
        {
            if (id != postDto.Id)
            {
                return BadRequest();
            }

            Post post = FromDto(postDto);
            post.UpdatedAt = DateTime.UtcNow;
            _context.Entry(post).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                    return NotFound();
                else
                    throw;
            }

            return NoContent();
        }

        // POST: api/Posts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PostOutDto>> PostPost(PostInDto postDto)
        {
            Post post = FromDto(postDto);
            post.CreatedAt = DateTime.UtcNow;
            post.UpdatedAt = DateTime.UtcNow;
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            PostOutDto postOutDto = ToOutDto(post);

            return CreatedAtAction("GetPost", new { id = postOutDto.Id }, postOutDto);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }

        private Post FromDto(PostInDto postDto)
        {
            Post post = new Post();
            post.Id = postDto.Id;
            post.Title = postDto.Title;
            post.Subtitle = postDto.Subtitle;
            foreach(PostContentDto contentDto in postDto.Contents)
                post.Contents.Add(FromDto(contentDto));
            return post;
        }

        private PostContent FromDto(PostContentDto postContentDto)
        {
            PostContent content = new PostContent();
            content.Id = postContentDto.Id;
            content.Title = postContentDto.Title;
            content.Content = postContentDto.Content;
            return content;
        }
        
        private PostBasicDto ToBasicDto(Post post)
        {
            PostBasicDto dto = new PostBasicDto();
            dto.Id = post.Id;
            dto.Title = post.Title;
            dto.Subtitle = post.Subtitle;
            return dto;
        }

        private PostCommentDto ToDto(PostComment comment)
        {
            PostCommentDto dto = new PostCommentDto();
            dto.Id = comment.Id;
            dto.Title = comment.Title;
            dto.Content = comment.Content;
            dto.CreatedAt = comment.CreatedAt;
            dto.PostId = comment.Post.Id;
            return dto;
        }

        private PostContentDto ToDto(PostContent content)
        {
            PostContentDto dto = new PostContentDto();
            dto.Id = content.Id;
            dto.Title = content.Title;
            dto.Content = content.Content;
            dto.PostId = content.Post.Id;
            return dto;
        }

        private PostOutDto ToOutDto(Post post)
        {
            PostOutDto dto = new PostOutDto();
            dto.Id = post.Id;
            dto.Title = post.Title;
            dto.Subtitle = post.Subtitle;
            dto.Slug = post.Slug;
            dto.CreatedAt = post.CreatedAt;
            dto.UpdatedAt = post.UpdatedAt;
            foreach(PostContent content in post.Contents)
                dto.Contents.Add(ToDto(content));
            foreach(PostComment comment in post.Comments)
                dto.Comments.Add(ToDto(comment));
            return dto;
        }
    }
}
