using System.ComponentModel.DataAnnotations;

namespace Account.Models;

public class RegisterModel
{
    [Required]
    public String? FirstName { get; set; }

    [Required]
    public String? LastName { get; set; }

    [Required]
    public String? Email { get; set; }

    [Required]
    public String? Password { get; set; }
}