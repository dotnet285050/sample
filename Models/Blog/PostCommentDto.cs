using System;

namespace Blog.Models;

public class PostCommentDto
{
    public int Id { get; set; }
    public String? Title { get; set; }
    public String? Content { get; set; }
    public DateTime CreatedAt { get; set; }
    public int PostId { get; set; }
}