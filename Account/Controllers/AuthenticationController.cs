using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Account.Data;
using Account.Models;

namespace Account.Controllers;

[Route("api/Account/")]
[ApiController]
public class AuthenticationController : Controller
{
    private readonly UserManager<AppUser> _userManager;
    private readonly SignInManager<AppUser> _signManager;

    public AuthenticationController(UserManager<AppUser> userManager,
                        SignInManager<AppUser> signManager)
    {
        _userManager = userManager;
        _signManager = signManager;
    }

    // POST: api/Account/Login
    [HttpPost]
    [Route("Login")]
    public async Task<ActionResult<UserInfo>> PostLogin(LoginModel model)
    {
        var user = await _userManager.FindByEmailAsync(model.Email.ToUpper());
        if (user == null)
            return NotFound();

        var result = await _signManager.PasswordSignInAsync(user, model.Password, false, false);
        if(result != Microsoft.AspNetCore.Identity.SignInResult.Success)
            return Unauthorized();

        UserInfo info = new UserInfo();
        info.FromAuthUser(user);
        return info;
    }

    // POST: api/Account/Logout
    [HttpPost]
    [Route("Logout")]
    public async Task<IActionResult> PostLogout()
    {
        var user = await _userManager.GetUserAsync(User);
        if (user != null)
            await _signManager.SignOutAsync();
        return NoContent();
    }
}