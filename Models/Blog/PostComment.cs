using System.ComponentModel.DataAnnotations;

namespace Blog.Models;

public class PostComment
{
    [Key]
    public int Id { get; set; }
    [Required]
    [StringLength(maximumLength: 500, MinimumLength = 10)]
    public String? Title { get; set; }
    [Required]
    public String? Content { get; set; }
    [Required]
    public DateTime CreatedAt { get; set; }
    [Required]
    public Post Post { get; set; } = null!;
}
